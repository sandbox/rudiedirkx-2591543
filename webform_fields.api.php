<?php

/**
 * Implements hook_webform_fields_node_methods_info().
 */
function hook_webform_fields_node_methods_info() {
  // Webform nodes of type 'event' are being used in 'registration' entities of type 'event' in field 'field_custom_fields'.
  $info['event']['registration:event:field_custom_fields'] = array(
    'title' => 'Registration custom fields',
    'callback' => '_webform_fields_entity_registration',
  );

  // Webform nodes of type 'party' are being used in 'node_registration' entities of type 'party' in field 'field_custom_fields'.
  $info['party']['node_registration:party:field_custom_fields'] = array(
    'title' => 'Node Registration custom fields',
    'callback' => '_webform_fields_node_registration',
  );

  return $info;
}

/**
 * Callback for webform_fields to find the event node from a Registration entity.
 */
function _webform_fields_entity_registration($context) {
  $form_state = $context['form_state'];
  if (isset($form_state['registration'])) {
    $registration = $form_state['registration'];
    if (is_object($registration) && is_a($registration, 'Registration')) {
      // Only works for Node registrations, because the webform is a node.
      if ($registration->entity_type == 'node') {
        return node_load($registration->entity_id);
      }
    }
  }
}

/**
 * Callback for webform_fields to find the event node from a Node Registration entity.
 */
function _webform_fields_node_registration($context) {
  $form = $context['form'];
  if (isset($form['#registration'])) {
    $registration = $form['#registration'];
    if (is_object($registration) && is_a($registration, 'NodeRegistrationEntityClass')) {
      return node_load($registration->nid);
    }
  }
}

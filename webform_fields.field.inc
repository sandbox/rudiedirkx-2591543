<?php

/**
 * Implements hook_field_widget_info().
 */
function webform_fields_field_widget_info() {
  return array(
    'webform_fields_widget' => array(
      'label' => t('Webform components/fields'),
      'field types' => array('webform_submission_field'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function webform_fields_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  if ($widget['type'] == 'webform_fields_widget') {
    $source = 'widget';
    $entity_type = $instance['entity_type'];
    $field_name = $instance['field_name'];
    $bundle = $instance['bundle'];
    $context = compact('source', 'entity_type', 'bundle', 'field_name', 'form', 'form_state', 'field', 'instance', 'langcode', 'items', 'delta', 'element');
    $node = _webform_fields_node($context);
    if ($node) {
      $item = @$items[$delta];
      $sid = @$item['sid'];
      $submission = $sid ? webform_menu_submission_load($sid, $node->nid) : FALSE;

      // Create fields wrapper. A form alter (or field/instance config?) might undo this.
      $element['submission'] = array('#type' => 'value', '#value' => $submission);
      $element['node'] = array('#type' => 'value', '#value' => $node);
      $element['component_data'] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => $instance['label'],
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );
      $element['component_data']['_description'] = array(
        '#markup' => t('Current sid: @sid', array('@sid' => $sid)),
        '#prefix' => '<div class="fieldset-description">',
        '#suffix' => '</div>',
        '#weight' => -100,
        '#access' => user_access('debug webform submission field'),
      );

      // Use partial webform validation via custom validation handler.
      $element['#element_validate'][] = 'webform_fields_webform_client_form_validate';

      // Prepare webform components.
      $form_state['webform']['component_tree'] = array();
      $page_count = 1;
      $page_num = 1;
      _webform_components_tree_build($node->webform['components'], $form_state['webform']['component_tree'], 0, $page_count);
      $component_tree = $form_state['webform']['component_tree'];

      if ($component_tree) {
        // This processor apparently loads all component dependencies.
        $form['#process'][] = 'webform_client_form_includes';

        // Comment stolen from Webform:
        // Recursively add components to the form. The unfiltered version of the form (typically used in Form Builder), includes all components.
        $filter = TRUE;
        foreach ($component_tree['children'] as $cid => $component) {
          $component_value = isset($form_state['values']['submitted'][$cid]) ? $form_state['values']['submitted'][$cid] : NULL;
          if ($filter == FALSE || _webform_client_form_rule_check($node, $component, $page_num, $form_state)) {
            _webform_client_form_add_component($node, $component, $component_value, $element['component_data'], $form, $form_state, $submission, 'form', $page_num, $filter);
          }
        }

        drupal_alter('webform_fields_webform_client_element', $element['component_data'], $form_state);

        return $element;
      }
    }
  }
}

/**
 * Validation handler for forms with a webform_fields_widget widget.
 */
function webform_fields_webform_client_form_validate($element, &$form_state, $form) {
  _webform_client_form_validate($element['component_data'], $form_state);
}

/**
 * Implements hook_webform_submission_field_presave().
 */
function webform_fields_webform_submission_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  if ($widget['type'] == 'webform_fields_widget') {
    foreach ($items as $delta => $item) {
      if (isset($item['component_data'])) {
        $submission = @$item['submission'];
        $node = @$item['node'];

        module_load_include('inc', 'webform', 'includes/webform.submissions');
        module_load_include('inc', 'webform', 'includes/webform.components');

        $data = $item['component_data'];
        $data = _webform_client_form_submit_flatten($node, $data);
        $data = webform_submission_data($node, $data);

        // Update existing.
        if ($submission) {
          $submission->data = $data + $submission->data;
          webform_submission_update($node, $submission);

          $sid = $submission->sid;
        }

        // Insert new.
        else {
          global $user;
          $submission = (object) array(
            'nid' => $node->nid,
            'uid' => $user->uid,
            'submitted' => REQUEST_TIME,
            'remote_addr' => ip_address(),
            'is_draft' => FALSE,
            'data' => $data,
          );
          $sid = webform_submission_insert($node, $submission);
        }

        // Save sid into field item value.
        $items[$delta]['sid'] = $sid;
      }
    }
  }
}

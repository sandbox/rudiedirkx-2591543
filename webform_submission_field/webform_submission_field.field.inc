<?php

/**
 * Implements hook_field_info().
 */
function webform_submission_field_field_info() {
  return array(
    'webform_submission_field' => array(
      'label' => t('Webform Submission'),
      'description' => t('Webform Submission reference'),
      'default_widget' => 'webform_submission_field_widget',
      'default_formatter' => 'webform_submission_field_formatter',
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function webform_submission_field_field_is_empty($item, $field) {
  if ($field['type'] == 'webform_submission_field') {
    $empty = FALSE;
    if (isset($item['wrapper'])) {
      $values = $item['wrapper'];
      // unset($values['sid']);
      $empties = array_filter(array_map(function($value) {
        return is_array($value) ? !array_filter($value) : (string) $value === '';
      }, $values));
      $empty = count($values) == count($empties);
    }
    // I don't know which widget uses this field. Call for ALL modules or have a generic enough is-empty algorithm?
    // drupal_alter('webform_submission_field_is_empty', $empty, $item, $field);
    return $empty;
  }
}

/**
 * Implements hook_field_widget_info().
 */
function webform_submission_field_field_widget_info() {
  return array(
    'webform_submission_field_widget' => array(
      'label' => t('Textfield with sid'),
      'field types' => array('webform_submission_field'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function webform_submission_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  if ($widget['type'] == 'webform_submission_field_widget') {
    $item = @$items[$delta];
    $element['wrapper'] = array(
      '#type' => 'fieldset',
      '#title' => $instance['label'],
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $element['wrapper']['sid'] = array(
      '#type' => 'textfield',
      '#default_value' => @$item['sid'],
      '#attributes' => array('placeholder' => 'sid'),
    );
    return $element;
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function webform_submission_field_field_formatter_info() {
  return array(
    'webform_submission_field_formatter' => array(
      'label' => t('Rendered submission'),
      'field types' => array('webform_submission_field'),
      'settings' => array(
        'fieldset' => 1,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function webform_submission_field_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element['fieldset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show fieldset?'),
    '#default_value' => !empty($settings['fieldset']),
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function webform_submission_field_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  return t('<strong>Fieldset</strong>: @yesno', array('@yesno' => $settings['fieldset'] ? t('Yes') : t('No')));
}

/**
 * Implements hook_field_formatter_view().
 */
function webform_submission_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $settings = $display['settings'];

  module_load_include('inc', 'webform', 'includes/webform.submissions');

  $output = array_map(function($item) use ($settings) {

    $sid = $item['sid'];
    $submission = webform_get_submissions(array('sid' => $sid));
    if (isset($submission[$sid])) {
      $submission = $submission[$sid];
      $node = node_load($submission->nid);

      // Render Webform-style.
      $rendered = webform_submission_render($node, $submission, NULL, 'html');
      $output = array(
        'sid' => array(
          '#markup' => t('Current sid: @sid', array('@sid' => $sid)),
          '#weight' => -100,
          '#access' => user_access('debug webform submission field'),
        ),
        'content' => array('#markup' => $rendered),
      );

      // Add fieldset according to formatter settings.
      if ($settings['fieldset']) {
        $output['#type'] = 'fieldset';
      }

      return $output;
    }

    // Invalid submission.
    return array();

  }, $items);

  return $output;
}

/**
 * Implements hook_field_presave().
 */
function webform_submission_field_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  if ($field['type'] == 'webform_submission_field') {
    // Pluck values from 'wrapper'.
    if (isset($items[key($items)]['wrapper'])) {
      $items = array_map(function($item) {
        $item = $item['wrapper'] + $item;
        unset($item['wrapper']);
        return $item;
      }, $items);
    }

    // The rest is up to the widget.
    $hook = $widget['module'] . '_webform_submission_field_presave';
    if (is_callable($hook)) {
      $hook($entity_type, $entity, $field, $instance, $langcode, $items);
    }
  }
}
